package server

import (
	"html/template"
	"net/http"

	"0xacab.org/sindominio/lowry/db"
	"0xacab.org/sindominio/lowry/ldap"
	"0xacab.org/sindominio/lowry/mail"
	"github.com/gorilla/mux"
)

type server struct {
	ldap         *ldap.Ldap
	mail         *mail.Mail
	db           *db.DB
	sess         *sessionStore
	tmpl         *template.Template
	usersAskRole []string
}

// Serve lowry web site
func Serve(addr string, l *ldap.Ldap, m *mail.Mail, ldb *db.DB, usersAskRole []string) error {
	s := server{
		ldap:         l,
		mail:         m,
		db:           ldb,
		sess:         initSessionStore(),
		tmpl:         initTemplate(),
		usersAskRole: usersAskRole,
	}

	r := mux.NewRouter()
	var notFoundFunc http.HandlerFunc
	notFoundFunc = s.notFoundHandler
	r.NotFoundHandler = notFoundFunc

	r.HandleFunc("/", s.homeHandler)
	r.HandleFunc("/login/", s.loginHandler)
	r.HandleFunc("/logout/", s.logoutHandler).Methods("POST")
	r.HandleFunc("/password/", s.passwordHandler)
	r.HandleFunc("/users/", s.usersHandler)
	r.HandleFunc("/users/{name}", s.userHandler)
	r.HandleFunc("/users/{name}/role/", s.roleHandler).Methods("POST")
	r.HandleFunc("/users/{name}/password/", s.passwdadmHandler).Methods("POST")
	r.HandleFunc("/users/{name}/shell/", s.shellHandler).Methods("POST")
	r.HandleFunc("/invites/", s.listInvitesHandler)
	r.HandleFunc("/invites/{invite}/del/", s.deleteInviteHandler)
	r.HandleFunc("/adduser/", s.createInviteHandler)
	r.HandleFunc("/adduser/{invite}", s.addUserHandler)
	r.HandleFunc("/groups/", s.groupsHandler)
	r.HandleFunc("/groups/{name}", s.groupHandler)
	r.HandleFunc("/groups/{name}/add/", s.addUserGroupHandler).Methods("POST")
	r.HandleFunc("/groups/{name}/del/", s.delUserGroupHandler).Methods("POST")
	r.HandleFunc("/ask-sindominante/", s.askSindominante).Methods("POST")

	r.HandleFunc("/bundle.js", func(w http.ResponseWriter, r *http.Request) { http.ServeFile(w, r, "dist/bundle.js") })
	r.HandleFunc("/style.css", func(w http.ResponseWriter, r *http.Request) { http.ServeFile(w, r, "dist/style.css") })
	r.HandleFunc("/js/jquery.dynatable.js", func(w http.ResponseWriter, r *http.Request) { http.ServeFile(w, r, "dist/js/jquery.dynatable.js") })
	r.HandleFunc("/js/jquery.dynatable.css", func(w http.ResponseWriter, r *http.Request) { http.ServeFile(w, r, "dist/js/jquery.dynatable.css") })
	r.HandleFunc("/js/zxcvbn.js", func(w http.ResponseWriter, r *http.Request) { http.ServeFile(w, r, "dist/js/zxcvbn.js") })
	r.HandleFunc("/js/zxcvbn-bootstrap-strength-meter.js", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "dist/js/zxcvbn-bootstrap-strength-meter.js")
	})
	r.Handle("/img/{img}", http.StripPrefix("/img/", http.FileServer(http.Dir("img"))))

	return http.ListenAndServe(addr, r)
}

func (s *server) notFoundHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	response := s.newResponse("404", w, r)
	response.execute(nil)
}

func (s *server) forbiddenHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusForbidden)
	response := s.newResponse("403", w, r)
	response.execute(nil)
}

func (s *server) errorHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusInternalServerError)
	response := s.newResponse("500", w, r)
	response.execute(nil)
}
