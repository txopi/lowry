package main

import (
	"bufio"
	"log"
	"os"
	"sort"
	"strings"
	"time"

	"0xacab.org/sindominio/lowry/db"
	"0xacab.org/sindominio/lowry/ldap"
	"0xacab.org/sindominio/lowry/mail"
	"0xacab.org/sindominio/lowry/server"
	"github.com/namsral/flag"
)

var (
	inviteExpireDuration  = time.Hour * 24 * 30 // 30 days
	accountExpireDuration = time.Hour * 24 * 90 // 90 days
)

func main() {
	var (
		ldapaddr    = flag.String("ldapaddr", "localhost:389", "LDAP server address and port")
		domain      = flag.String("domain", "", "LDAP domain components")
		ldappass    = flag.String("ldappass", "", "Password of the LDAP `admin' user")
		homepath    = flag.String("homepath", "/home/", "Path to the user homes")
		smtpaddr    = flag.String("smtpaddr", "localhost:25", "The address of the smtp server to send email")
		email       = flag.String("email", "", "The email address to send notifications from")
		emailpass   = flag.String("emailpass", "", "The password of the email address")
		httpaddr    = flag.String("httpaddr", ":8080", "Web server address and port")
		dbpath      = flag.String("dbpath", "bolt.db", "The path to store the lowry status database")
		ro          = flag.Bool("ro", false, "Read-Only mode")
		askRolePath = flag.String("ask-role-list", "", "List of usernames to ask if want to be sindominantes")
	)
	flag.String(flag.DefaultConfigFlagname, "/etc/lowry.conf", "Path to configuration file")
	flag.Parse()

	m := mail.Init(*email, *emailpass, *smtpaddr, *domain)
	l := ldap.Ldap{
		Addr:     *ldapaddr,
		Domain:   *domain,
		Pass:     *ldappass,
		HomePath: *homepath,
		RO:       *ro,
	}
	err := l.Init()
	if err != nil {
		log.Fatal(err)
	}

	ldb, err := db.Init(*dbpath)
	if err != nil {
		log.Fatal(err)
	}
	defer ldb.Close()
	go cleanInvites(ldb)

	usersAskRole := []string{}
	if *askRolePath != "" {
		usersAskRole = readUserList(*askRolePath)
	}

	log.Fatal(server.Serve(*httpaddr, &l, m, ldb, usersAskRole))
}

func cleanInvites(ldb *db.DB) {
	for {
		ldb.ExpireInvites(inviteExpireDuration)
		ldb.ExpireAccounts(accountExpireDuration)
		time.Sleep(time.Minute * 60)
	}
}

func readUserList(listPath string) []string {
	f, err := os.Open(listPath)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	list := []string{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		user := strings.TrimSpace(scanner.Text())
		list = append(list, user)
	}
	sort.Strings(list)
	return list
}
